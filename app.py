import logging
import os
import threading
import time

import requests
import schedule
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait

logging.basicConfig(format="%(asctime)-15s %(levelname)-8s %(message)s")
logger = logging.getLogger()
logger.setLevel('INFO')
game_types = ['ATP - SINGLES', 'ATP - DOUBLES', 'CHALLENGER MEN', 'CHALLENGER MEN - DOUBLES', 'EXHIBITION - MEN',
              'CHALLENGER MEN - SINGLES', 'ITF MEN - SINGLES', 'ITF MEN - DOUBLES']


def telegram_bot_sendtext(game_data):
    message = f"*Home Player* -> {game_data.get('home_player')} \n" \
              f"*Away Player* -> {game_data.get('away_player')} \n" \
              f"*Tournament Type* -> {game_data.get('torn').capitalize()} \n" \
              f"*Set* -> {game_data.get('set')} \n" \
              f"*Away Score* -> {game_data.get('away_score')} \n" \
              f"*Home Score* -> {game_data.get('home_score')}\n" \
              f"*Game Link* -> {game_data.get('game_link')}"
    bot_token = '851841059:AAHLxXoDYFsR3RGXk0D1gZBc8VFpl_oTftk'
    bot_chat_ids = [383408179, 350272735]
    for chat in bot_chat_ids:
        send_text = 'https://api.telegram.org/bot' + bot_token + '/sendMessage?chat_id=' + str(
            chat) + '&parse_mode=Markdown&text=' + message
        response = requests.get(send_text)
        logger.info(response.json())

    # return response.json()


def get_attributes(driver, element):
    attributes = driver.execute_script(
        'var items = {}; for (index = 0; index < arguments[0].attributes.length; ++index) { items[arguments[0].attributes[index].name] = arguments[0].attributes[index].value }; return items;',
        element)
    return attributes


def get_scheduled(driver):
    games = []

    live_table = driver.find_element_by_id('live-table')
    event_table = live_table.find_element_by_class_name('event')
    tennis_sport = event_table.find_element_by_class_name('sportName.tennis')
    games_ = tennis_sport.find_elements_by_xpath(".//*")

    current_event_type = ''

    for element in games_:
        try:
            attrs = get_attributes(driver, element)

            class_attr = attrs.get('class')

            if class_attr == 'event__header' or class_attr == 'event__header top':

                game_type = element.find_element_by_class_name('event__title--type').text

                if current_event_type == '':

                    logger.info(f"Changing current game type to {game_type}")
                    current_event_type = game_type
                    continue

                else:
                    if current_event_type != game_type:
                        logger.info(f"Changing current game type to {game_type}")
                        current_event_type = game_type
                        continue

            # data = {}
            if current_event_type != '':
                if current_event_type in game_types:
                    try:
                        event_time = element.find_element_by_class_name('event__time').text
                        if "FRO" in event_time:
                            continue
                        game_time = event_time

                        home_player = element.find_element_by_class_name(
                            'event__participant.event__participant--home').text
                        away_player = element.find_element_by_class_name(
                            'event__participant.event__participant--away').text
                        game_id = attrs.get('id')

                        data = {
                            'home_player': home_player,
                            'away_player': away_player,
                            'game_id': game_id,
                            'live': False,
                            'game_time': game_time,
                            'game_link': f'https://www.flashscore.co.ke/match'
                                         f'/{game_id.replace("g_2_", "").strip()}/#match-summary',
                            'torn': current_event_type
                        }

                        logger.info(f"adding {data} from {current_event_type}")
                        games.append(data)

                        # print(data)
                    except NoSuchElementException as e:
                        # print(attrs.get('class'))
                        if 'event__match event__match--live event__match--twoLine' == attrs.get(
                                'class') or 'event__match event__match--live event__match--last ' \
                                            'event__match--twoLine' == attrs.get('class'):
                            home_player = element.find_element_by_class_name(
                                'event__participant.event__participant--home').text

                            away_player = element.find_element_by_class_name(
                                'event__participant.event__participant--away').text

                            game_id = attrs.get('id')

                            data = {
                                'home_player': home_player,
                                'away_player': away_player,
                                'game_id': game_id,
                                'live': True,
                                'game_link': f'https://www.flashscore.co.ke/match'
                                             f'/{game_id.replace("g_2_", "").strip()}/#match-summary',
                                'torn': current_event_type
                            }
                            logger.info(f"adding {data} from {current_event_type}")
                            games.append(data)

                            # print('live')
        except (StaleElementReferenceException, NoSuchElementException):
            time.sleep(5)
            continue

    return games


def collect_games():
    driver_ = load_driver()

    url = 'https://www.flashscore.co.ke/tennis/'
    driver_.get(url)
    today_games = get_scheduled(driver_)
    # today_games = check_live(driver_)
    driver_.quit()
    return today_games


def run_threaded(job_func, game_data):
    job_thread = threading.Thread(target=job_func, args=(game_data,))
    job_thread.start()


def schedule_games(games):
    for game in games:
        game_time = game.get('game_time')
        is_live = game.get('live')
        if is_live:
            # run_at(game)
            run_threaded(run_at, game)
        else:
            logger.info(f"Scheduling game {game.get('game_link')} at {game_time}")
            schedule.every().day.at(game_time).do(run_threaded, run_at, game_data=game)
            # pass


def element_class_name(driver, name):
    return WebDriverWait(driver, 10).until(ec.presence_of_element_located((By.CLASS_NAME, name)))


def extract_breaks(driver_):
    main_tab = driver_.find_element_by_class_name('matchHistoryWrapper___2jjRzgy')

    game_divs = main_tab.find_elements_by_xpath(".//*")

    break_count = 0
    for element in game_divs:
        attrs = get_attributes(driver_, element)
        if attrs.get('class'):
            if attrs.get('class') == 'bold___1xvstc4':
                break_count += 1

    return break_count


def extract_score(driver_, set_):
    scores = driver_.find_element_by_xpath('//*[@id="detail"]/div[4]/div[3]/div/div[2]/span[2]').text
    scores = scores.split(":")

    home_score = scores[0]
    away_score = scores[1]

    return {
        'home_score': home_score,
        'away_score': away_score
    }


def load_driver():
    options = Options()
    options.headless = True

    firefox_profile = webdriver.FirefoxProfile()
    firefox_profile.set_preference('browser.download.folderList', 2)
    firefox_profile.set_preference('browser.download.manager.showWhenStarting', False)
    firefox_profile.set_preference('browser.download.dir', os.getcwd())
    firefox_profile.set_preference('browser.helperApps.neverAsk.saveToDisk', 'text/csv')

    logging.info('Prepared firefox profile..')

    driver_ = webdriver.Firefox(firefox_profile=firefox_profile, options=options)
    logging.info('Initialized firefox browser..')
    logging.info('Web driver started')
    # print('Web driver started')

    logging.info('Web driver started')
    return driver_


def run_at(game_data):
    logger.info(f'game {game_data.get("game_link")} running i thread {threading.current_thread()}')

    logging.info(game_data)
    driver_ = load_driver()
    url = game_data.get('game_link')

    driver_.get(url)
    current_set = 0

    while True:
        try:
            time.sleep(5)
            status_div = element_class_name(driver_, 'status___XFO5ZlK')
            status = status_div.text
            if status:
                if status.lower() == 'finished':
                    logger.warning(f"Game {url}  has ended")
                    break
                else:
                    logger.warning(f"Game {url}  is  running")

                    status = status_div.find_element_by_class_name('detailStatus___2v20X7g').text
                    if "set 1" in status.lower():

                        if current_set != 1:
                            current_set = 1
                            logger.info("in set 1")
                            set_1_suffix = 'match-summary/point-by-point/0'
                            set_1_url = url.split("#")[0]
                            set_1_url = set_1_url + "#" + set_1_suffix
                            print(set_1_url)
                            driver_.get(set_1_url)
                        else:
                            set_1_breaks = extract_breaks(driver_)
                            set_1_scores = extract_score(driver_, current_set)
                            home_score = set_1_scores.get('home_score')
                            away_score = set_1_scores.get('away_score')

                            if int(set_1_breaks) == 2 and int(home_score) == 5 and int(away_score) == 5:
                                logger.info("PING")
                                game_data["set"] = current_set
                                game_data["away_score"] = away_score
                                game_data["home_score"] = home_score
                                logger.info(game_data)
                                telegram_bot_sendtext(game_data)
                                time.sleep(600)
                            elif int(set_1_breaks) > 3 or int(home_score) > 5 or int(away_score) > 5:
                                logger.info(
                                    f"Game {url}  at score {home_score} - {away_score} breaks {set_1_breaks} at set 1")
                                logger.info(
                                    f"Game {url} breaks above odds sleeping for 5 minutes")
                                time.sleep(600)
                                continue
                            else:
                                logger.info(
                                    f"Game {url}  at score {home_score} - {away_score} breaks {set_1_breaks} at set 1")

                    elif "set 3" in status.lower():

                        if current_set != 3:
                            current_set = 3
                            logger.info("in set 3")
                            set_3_suffix = 'match-summary/point-by-point/2'
                            set_3_url = url.split("#")[0]
                            set_3_url = set_3_url + '#' + set_3_suffix
                            print(set_3_url)

                            driver_.get(set_3_url)

                        else:
                            set_3_breaks = extract_breaks(driver_)
                            set_3_scores = extract_score(driver_, current_set)
                            home_score = set_3_scores.get('home_score')
                            away_score = set_3_scores.get('away_score')

                            if int(set_3_breaks) == 2 and int(home_score) == 5 and int(away_score) == 5:
                                logger.info("PING")
                                game_data["set"] = current_set
                                game_data["away_score"] = away_score
                                game_data["home_score"] = home_score
                                logger.info(game_data)
                                telegram_bot_sendtext(game_data)
                                time.sleep(600)
                            elif int(set_3_breaks) > 3 or int(home_score) > 5 or int(away_score) > 5:
                                logger.info(
                                    f"Game {url}  at score {home_score} - {away_score} breaks {set_3_breaks} at set 3")
                                logger.info(
                                    f"Game {url} breaks above odds sleeping for 5 minutes")
                                time.sleep(600)
                                return True
                            else:
                                logger.info(
                                    f"Game {url}  at score {home_score} - {away_score} breaks {set_3_breaks} at set 3")
                    else:
                        logger.info(f"sleeping for 2 min for game {url}")
                        time.sleep(2 * 60)

            else:
                logger.warning(f"Game {url}  has not yet started")
                time.sleep(15)
        except Exception as e:
            logger.warning(e)
            driver_.refresh()
            time.sleep(6)

    driver_.close()
    return schedule.CancelJob


def run():
    collected_games = collect_games()
    schedule_games(collected_games)


if __name__ == '__main__':
    run()
    schedule.every().day.at("01:00").do(run)

    while 1:
        schedule.run_pending()
        time.sleep(1)
